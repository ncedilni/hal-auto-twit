# What is this?!

A python command-line tool to automatically tweet the latest publications from
your *Équipe projet Inria*. It may work for other institutions, as long as the
publications are referenced in [HAL](https://hal.archives-ouvertes.fr/).

# How can I use it?

The script should be run at an appropriate frequency. At
[Asclepios](https://team.inria.fr/asclepios), we run it once a day using a
simple cronjob.

# What are the requirements?

- Python >= 3.6, because I love the format strings syntax.
- [twitter](https://pypi.python.org/pypi/twitter), a pypi package because I was
 too lazy to read the twitter API documentation.
- [python-dateutil](https://pypi.python.org/pypi/python-dateutil), because date
 strings in HAL metadata are hard to parse.

# How do I login to twitter? 

You must register an [app](https://apps.twitter.com/) on twitter and use its
credential to login.

# Command-line options

```
usage: autotwit.py [-h] (--epi-name EPI_NAME | --structure-id STRUCTURE_ID)
                   [--history-file HISTORY_FILE] [--hal-url HAL_URL]
                   [--no-tweet | --tweet]
                   [--no-write-history | --write-history] [--log LOG]
                   token token_secret consumer_key consumer_secret

positional arguments:
  token                 Twitter app access token
  token_secret          Twitter app access token secret
  consumer_key          Twitter consumer key
  consumer_secret       Twitter consumer secret

optional arguments:
  -h, --help            show this help message and exit
  --epi-name EPI_NAME   The EPI name, that will be used in the "Research
                        structure(s) 'contain'" field of HALtools export
  --structure-id STRUCTURE_ID
                        Alternatively, the HAL structure ID of the EPI.
  --history-file HISTORY_FILE
                        The history file, used to store URLs that were already
                        tweeted. Defaults to ~/.autotwit_hist
  --hal-url HAL_URL     HAL export publications URL. You shouldn't need to
                        change this.
  --no-tweet            Do not tweet anything. Useful to create a history file
                        in order not to tweet all previous publications
  --tweet               Actually tweet (default behaviour)
  --no-write-history    Do not write history. Beware! This will lead to
                        multiple tweets for the same document if not used
                        cautiously.
  --write-history       Write tweet history (default behaviour)
  --log LOG             Verbosity level
```

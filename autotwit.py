#!/usr/bin/env python

import logging
import xml.etree.ElementTree as ET
import requests
import os.path
import sys
from datetime import datetime
from os import environ

import dateutil.parser
from twitter import Twitter, OAuth

log = logging.getLogger(__name__)


def get_hal_xml(hal_url, epi_name=None, structure_id=None):
    if (epi_name is None and structure_id is None
            or epi_name is not None and structure_id is not None):
        raise TypeError("Specify either epi_name or structure")
    log.info("Interrogating HAL")
    hal_params = {"annee_publideb": str(datetime.today().year),
                  "format_export": "xml",
                  "langue": "Anglais"}
    if epi_name is not None:
        hal_params.update({"labos_exp": epi_name})
    if structure_id is not None:
        hal_params.update({"struct": structure_id})
    request = requests.get(hal_url, params=hal_params)
    log.debug(f"HAL response: {request.status_code}")

    if request.status_code != 200:
        raise RuntimeError("HAL status: {request.status_code}")

    return request.text


def parse_hal_xml(text, hist_list):
    log.info("Parsing XML into publications")
    root = ET.fromstring(text)
    ns = {'tei': 'http://www.tei-c.org/ns/1.0'}

    text = root.find('tei:text', ns)
    body = text.find('tei:body', ns)
    list_bibl = body.find('tei:listBibl', ns)

    publis = []
    for bibl_full in list_bibl:
        publication_stmt = bibl_full.find('tei:publicationStmt', ns)
        url = [u for u in publication_stmt.findall('tei:idno', ns)
               if u.get('type') == 'halUri'][0].text

        if url in hist_list:
            log.debug(f"{url} found in history, skipping")
            continue

        title_stmt = bibl_full.find('tei:titleStmt', ns)
        title = title_stmt.find('tei:title', ns).text

        edition_stmt = bibl_full.find('tei:editionStmt', ns)
        edition = edition_stmt.find('tei:edition', ns)

        date_sub = [d for d in edition.findall('tei:date', ns)
                    if d.get('type') == 'whenSubmitted'][0]
        date_sub = datetime.strptime(date_sub.text, '%Y-%m-%d %H:%M:%S')

        date_produced = [d for d in edition.findall('tei:date', ns)
                         if d.get('type') == 'whenProduced']
        if date_produced:
            log.debug(f"Date_produced: {date_produced[0].text} for {title}")
            date_produced = dateutil.parser.parse(date_produced[0].text)
            if date_produced > datetime.today():
                log.debug("Production date in the future, "
                          "skipping publication")
                continue

        publis += [{'title': title, 'date': date_sub, 'url': url}]
        log.debug(f"Title: {title}\n"
                  f"Date: {date_sub}\n"
                  f"URL: {url}")

    publis.sort(key=lambda x: x['date'])

    return publis


def parse_history(file_name):
    try:
        with open(file_name, 'r') as fh:
            res = fh.read().strip().split("\n")
    except FileNotFoundError:
        res = []
    return res


def twit_it(publis, token, token_secret, consumer_key, consumer_secret,
            hist_file, do_twit, do_write):
    if do_twit:
        twitter = Twitter(auth=OAuth(token, token_secret,
                                     consumer_key, consumer_secret))
    for pub in publis:
        message = f"{pub['title']} {pub['url']}"
        if do_twit:
            log.info(f"Tweeting '{message}'")
            twitter.statuses.update(status=message)
        if do_write:
            with open(hist_file, 'a') as fh:
                print(pub['url'], file=fh)


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("token", help="Twitter app access token")
    parser.add_argument("token_secret",
                        help="Twitter app access token secret")
    parser.add_argument("consumer_key",
                        help="Twitter consumer key")
    parser.add_argument("consumer_secret",
                        help="Twitter consumer secret")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--epi-name",
        help="The EPI name, that will be used in the "
             "\"Research structure(s) \'contain\'\" field of HALtools export")
    group.add_argument(
        "--structure-id",
        help="Alternatively, the HAL structure ID of the EPI.")
    parser.add_argument("--history-file",
                        default=os.path.join(environ['HOME'],
                                             '.autotwit_hist'),
                        help="The history file, used to store URLs that were "
                             "already tweeted. Defaults to ~/.autotwit_hist")
    parser.add_argument(
        "--hal-url",
        default="https://haltools.inria.fr/Public/exportPubli.php",
        help="HAL export publications URL. You shouldn't need to change this."
    )

    do_twit_parser = parser.add_mutually_exclusive_group(required=False)
    do_twit_parser.add_argument('--no-tweet', dest='tweet',
                                action='store_false',
                                help='Do not tweet anything. Useful to create '
                                     'a history file in order not to tweet all'
                                     ' previous publications')
    do_twit_parser.add_argument('--tweet', dest='tweet',
                                action='store_true',
                                help="Actually tweet (default behaviour)")
    parser.set_defaults(tweet=True)

    do_write_parser = parser.add_mutually_exclusive_group(required=False)
    do_write_parser.add_argument('--no-write-history', dest='write_history',
                                 action='store_false',
                                 help='Do not write history. Beware! This will'
                                      ' lead to multiple tweets for the same '
                                      'document if not used cautiously.')
    do_write_parser.add_argument('--write-history', dest='write_history',
                                 action='store_true',
                                 help="Write tweet history (default behaviour)")
    parser.set_defaults(write_history=True)

    parser.add_argument("--log", default="INFO", help="Verbosity level")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f'Invalid log level: {args.log}')
    logging.basicConfig(level=numeric_level)

    for key, value in vars(args).items():
        log.debug(f"CLI argument '{key}': {value}")

    history = parse_history(args.history_file)
    hal_xml = get_hal_xml(args.hal_url,
                          epi_name=args.epi_name,
                          structure_id=args.structure_id)
    publications = parse_hal_xml(hal_xml, history)

    if len(publications) == 0:
        log.info("Nothing to tweet, exiting")
        sys.exit(0)

    twit_it(publications, args.token, args.token_secret, args.consumer_key,
            args.consumer_secret, args.history_file, args.tweet,
            args.write_history)
